using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms.DataVisualization.Charting;
using System.Linq;
// The Ink namespace, which contains the Tablet PC Platform API
using Microsoft.Ink;
using System.Windows.Ink;
using System.Windows.Input;

using PBT.Core;
using PBT.Core.TestObject;

namespace ClockReader
{
    public partial class ClockReader : Form
	{        
        // The one and only ink collector
		private InkCollector ic;

        private int[] scoreboard = new int[13];
        private float[] airtime;

        private FileStream file;
        private StreamReader sr;
        
        private double scale;
        private int DurationInSecond;
        private Point translationPt;        

        private string displayedFileName;
        private System.Collections.Generic.Dictionary<string, Clock> dicClocks;                

        // for sqlite 
        private SQLiteConnection sqlConnection;

        // for animation
        private bool isPlaying = false;        

        //load clock for scoring
        private Clock oClock = new Clock(); 

		// Constructor
		public ClockReader()
		{
            #region Standard Template Code
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            

            #endregion
            this.mInkOverly = new InkOverlay(this.ClockDrawing);

            // Create the InkCollector and attach it to the signature GroupBox
            ic = new InkCollector(this.ClockDrawing.Handle);
            //ic.Enabled = true;
            renderInk = new Ink();
            dicClocks = new System.Collections.Generic.Dictionary<string, Clock>();

            timer.Interval = 1;
            timer.Tick += new EventHandler(Application_Idle);
		}

        #region event handler
        void Application_Idle(object sender, EventArgs e)
        {
            if (this.dicClocks.Count == 0)
            {
                timer.Stop();
                MessageBox.Show("Please load a clock");
                return;
            }

            Animation();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClockDrawing_SizeChanged(object sender, EventArgs e)
        {
            if (ic != null)
            {
                Ink loadedInk = new Ink();
                loadedInk = ic.Ink.Clone();
                ic.Ink.DeleteStrokes(ic.Ink.Strokes);

                Rectangle scaleRectangle = new System.Drawing.Rectangle(0, 0, this.ClockDrawing.Height, this.ClockDrawing.Height);
                loadedInk.Strokes.ScaleToRectangle(scaleRectangle);
                loadedInk.Strokes.Scale(25, 25);
                // temporarily disable the ink collector and swap ink objects
                Rectangle boundRect = loadedInk.GetBoundingBox();

                ic.Enabled = false;
                ic.Ink = loadedInk;
                //ic.Enabled = true;

                this.ClockDrawing.Invalidate();
            }
        }

        //receive clock object from ScoringNumbers class and define myClock as clock

        private void saveClockToImageToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog savingDialog = new SaveFileDialog();
            savingDialog.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            savingDialog.Title = "Save an Image File";
            savingDialog.ShowDialog();

            if (savingDialog.FileName != "")
            {
                // create graphics object from the bitmap
                Bitmap b2 = new Bitmap(this.ClockDrawing.Image);
                Graphics g1 = Graphics.FromImage(b2);
                g1.SmoothingMode = SmoothingMode.AntiAlias;
                // make a renderer to draw ink on the graphics surface
                Renderer r = new Renderer();
                r.Draw(g1, ic.Ink.Strokes);

                b2.Save(savingDialog.FileName);
            }
        }

        private void menuCapture_Click(object sender, EventArgs e)
        {
            this.saveClockToImageToolStripMenuItem_Click(sender, e);
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            PrintingUtility ptu = new PrintingUtility(this.displayedFileName, this.ClockDrawing);
            ptu.Print();            
        }

		/// <summary>
        /// This function only open a file instead of a series of files in a folder.
        /// This function handles the Open... command. It will determine the type of ink which is being opened and call the appropriate helper routine.
        /// The try...catch section in this function will handle all of the error handling for the functions it calls.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void OpenMenu_Click(object sender, System.EventArgs e)
		{			
			/// Create the OpenFileDialog, which presents a standard Windows
			/// dialog to the user.
			OpenFileDialog openDialog = new OpenFileDialog();

			/// Set the filter to suggest our recommended extensions
            openDialog.Filter = "Ink Serialized Format files (*.isf)|*.isf";
 
			/// If the dialog exits and the user didn't choose Cancel
			if(openDialog.ShowDialog() == DialogResult.OK)
			{
                loadClockInk(Path.GetFullPath(openDialog.FileName));

                this.displayedFileName = openDialog.FileName.Substring(openDialog.FileName.LastIndexOf('\\') + 1);
                dispStrokesOnCanvas(dicClocks[displayedFileName]);
                ////add stuff here

                //
                //
                //

                //check the local score
                //have to pass penstrokes
                //Temporary workaround for getting a circle object to the scoring numbers algorithms
                //TODO re-implement this
                Circle myCircle = new Circle(ClockDrawing.Width / 2 - 365, ClockDrawing.Height / 2 - 365, 365 * 2); 
                //send local clock to scoringNumbers algo
                ScoringNumbers currentScore = new ScoringNumbers(oClock, myCircle);
                //score values
                int[] scoreBoard = currentScore.scoringCPH();
                dispScores(scoreBoard);


                dispStrokeOrder(dicClocks[displayedFileName].PenStrokes);
                reportAirTime(dicClocks[displayedFileName].PenStrokes);
                reportPressure(dicClocks[displayedFileName].PenStrokes);

                // display information
                this.lbID.Text = this.displayedFileName.Split('.')[0];

                long startingTime = dicClocks[displayedFileName].PenStrokes.First().PacketPoints.First().TimeStamp;
                long endingTime = dicClocks[displayedFileName].PenStrokes.Last().PacketPoints.Last().TimeStamp;
                long longDuration = endingTime - startingTime;
                this.DurationInSecond = (int)(longDuration / 1000);
                int dispSecond = DurationInSecond % 60;
                int dispMinute = (DurationInSecond - dispSecond) / 60;

                string dispDuration = dispMinute.ToString() + " minutes " + dispSecond.ToString() + " seconds ";
                this.lbDuration.Text = dispDuration;

                this.lbTrial.Text = dicClocks[displayedFileName].numOfTrial.ToString();
			}
		}

        /// <summary>
        /// This function open a folder that contains multiple records of clock drawings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog openDialog = new FolderBrowserDialog();

            DialogResult result = openDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                string folderPath = openDialog.SelectedPath;
                if (folderPath != "")
                {
                    string[] filePaths = Directory.GetFiles(folderPath, "*.isf");
                    foreach (string file in filePaths)
                    {

                    }
                }
            }
        }

        private void saveMenu_Click(object sender, EventArgs e)
        {
            /// Create a stream which will be used to save data to the output file
            Stream myStream = null;

            /// Create the SaveFileDialog, which presents a standard Windows
            /// Save dialog to the user.
            SaveFileDialog saveDialog = new SaveFileDialog();

            /// Set the filter to suggest our recommended extensions
            saveDialog.Filter = "Graphics Interchange Format files (*.ISF)|*.ISF";

            /// If the dialog exits and the user didn't choose Cancel
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    /// Attempt to Open the file with read/write permission
                    myStream = saveDialog.OpenFile();
                    if (myStream != null)
                    {
                        // Put the filename in a more canonical format
                        String filename = saveDialog.FileName.ToLower();

                        // Get a version of the filename without an extension
                        // This will be used for saving the associated image
                        String extensionlessFilename = Path.GetFileNameWithoutExtension(filename);

                        // Get the extension of the file 
                        String extension = Path.GetExtension(filename);

                        String filePath = filename.Replace(extensionlessFilename + extension, "");

                        saveISF(myStream);
                    }
                    else
                    {
                        // Throw an exception if a null pointer is returned for the stream
                        throw new IOException();
                    }
                }
                catch (IOException /*ioe*/)
                {
                    MessageBox.Show("File error");
                }
                finally
                {
                    // Close the stream in the finally clause so it
                    // is always reached, regardless of whether an 
                    // exception occurs.  SaveXML, SaveHTML, and
                    // SaveISF can throw, so this precaution is necessary.
                    if (null != myStream)
                    {
                        myStream.Close();
                    }
                }
            } // End if user chose OK from dialog 
        }

        private void saveISF(Stream myStream)
        {
            byte[] isf;

            // Perform the serialization
            isf = ic.Ink.Save(PersistenceFormat.InkSerializedFormat);

            // Write the ISF to the stream
            myStream.Write(isf, 0, isf.Length);
        }

        private void penButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void eraserButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void blackButton_CheckedChanged(object sender, EventArgs e)
        {
            ic.DefaultDrawingAttributes.Color = Color.Black;
        }

        private void redButton_CheckedChanged(object sender, EventArgs e)
        {
            ic.DefaultDrawingAttributes.Color = Color.Red;
        }

        private void greenButton_CheckedChanged(object sender, EventArgs e)
        {
            ic.DefaultDrawingAttributes.Color = Color.Green;
        }

        private void blueButton_CheckedChanged(object sender, EventArgs e)
        {
            ic.DefaultDrawingAttributes.Color = Color.Blue;
        }

        void lbReco_MouseEnter(object sender, EventArgs e)
        {
            if (!isPlaying)
            {
                Label lb = (Label)sender;
                int id = int.Parse(lb.Name);
                //PenStroke stroke = dicClocks[displayedFileName].PenStrokes[id];
                Microsoft.Ink.Stroke stroke = this.ic.Ink.Strokes[id];
                Rectangle boundingRect = stroke.GetBoundingBox();//stroke.PenStk.GetBoundingBox();
                Point upperLeft = new System.Drawing.Point(boundingRect.X, boundingRect.Y);
                Point bottomRight = new System.Drawing.Point(boundingRect.Right, boundingRect.Bottom);

                using (Graphics g = this.ClockDrawing.CreateGraphics())
                {
                    this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref upperLeft);
                    this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref bottomRight);
                    Rectangle drawingRect = new Rectangle(upperLeft, new System.Drawing.Size(bottomRight.X - upperLeft.X, bottomRight.Y - upperLeft.Y));

                    System.Drawing.Pen pen = new System.Drawing.Pen(Color.Red, 3);
                    g.DrawRectangle(pen, drawingRect);
                    //drawingRect.Inflate(5,5);
                    this.ClockDrawing.Invalidate(drawingRect);
                }
            }
        }

        void lbReco_MouseLeave(object sender, EventArgs e)
        {
            if (!isPlaying)
            {
                Label lb = (Label)sender;
                int id = int.Parse(lb.Name);
                //PenStroke stroke = dicClocks[displayedFileName].PenStrokes[id];
                Microsoft.Ink.Stroke stroke = this.ic.Ink.Strokes[id];
                Rectangle boundingRect = stroke.GetBoundingBox();//stroke.PenStk.GetBoundingBox();
                Point upperLeft = new System.Drawing.Point(boundingRect.X, boundingRect.Y);
                Point bottomRight = new System.Drawing.Point(boundingRect.Right, boundingRect.Bottom);
                using (Graphics g = this.ClockDrawing.CreateGraphics())
                {
                    this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref upperLeft);
                    this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref bottomRight);
                    Rectangle drawingRect = new Rectangle(upperLeft, new System.Drawing.Size(bottomRight.X - upperLeft.X, bottomRight.Y - upperLeft.Y));
                    drawingRect.Inflate(6, 6);
                    this.ClockDrawing.Invalidate(drawingRect);
                }
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            OpenMenu_Click(sender, e);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            saveMenu_Click(sender, e);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
        Timer timer = new Timer();

        private void play_pause_Click(object sender, EventArgs e)
        {
            if (!isPlaying)
            {
                this.mInkOverly.Ink.DeleteStrokes(this.mInkOverly.Ink.Strokes);
                this.ClockDrawing.Ink.DeleteStrokes(this.ClockDrawing.Ink.Strokes);

                isPlaying = true;

                long now = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);
                long difference = now - pauseTime;
                startTime = startTime + difference;

                timer.Start();

                this.play_pause.Image = Properties.Resources.media_pause;
            }
            else
            {
                this.play_pause.Image = Properties.Resources.play;
                isPlaying = false;
                pauseTime = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);

                timer.Stop();
            }
        }

        #endregion
        
        #region load data


        /// <summary>
        /// load a ISF file from a path
        /// </summary>
        /// <param name="path"></param>
        private void loadClockInk(string path)
        {
            /// Create a stream which will be used to load data from the output file
            FileStream myStream = null;
            try
            {
                /// Attempt to Open the file with read only permission
                myStream = File.OpenRead(path);
                if (myStream != null)
                {
                    // Put the filename in a more canonical format
                    String filename = path.Substring(path.LastIndexOf('\\')+1);
                    String filepath = path.Remove(path.LastIndexOf('\\') + 1);

                    // Get a version of the filename without an extension
                    // This will be used for saving the associated image
                    String extensionlessFilename = Path.GetFileNameWithoutExtension(filename);

                    string connectionString = @"Data Source=" + filepath + extensionlessFilename + "_sqlite.db3";
                    this.sqlConnection = new SQLiteConnection(connectionString);
                    this.sqlConnection.Open();

                    
                    oClock.Ink = loadISF(myStream);
                    this.LoadStrokes(oClock);
                    if (dicClocks.ContainsKey(filename))
                        dicClocks[filename] = oClock;
                    else
                        dicClocks.Add(filename, oClock);

                    createButton(oClock, filename);
                }
                else
                {
                    // Throw an exception if a null pointer is returned for the stream
                    throw new IOException();
                }
            }
            catch (IOException e)
            {
                MessageBox.Show("File error");
            }
            catch (Exception e)
            {
                // If the xml or the scanned form image file are not available,
                // display an error and exit
                MessageBox.Show("An error occured while loading ink from the specified file.\n" +
                    "Please verify that the file contains valid serialized ink and try again.",
                    "Serialization",
                    MessageBoxButtons.OK);
            }
            finally
            {
                // Close the stream in the finally clause so it
                // is always reached, regardless of whether an 
                // exception occurs.  LoadXML, LoadHTML, and
                // LoadISF can throw, so this precaution is necessary.
                if (null != myStream)
                {
                    myStream.Close();
                }
            }

        }

        // This function will load ISF into the ink object.
        // It will also pull the data stored in the object's extended properties and
        // repopulate the text boxes.
        private Ink loadISF(Stream s)
        {
            Ink loadedInk = new Ink();
            byte[] isfBytes = new byte[s.Length];

            // read in the ISF
            s.Read(isfBytes, 0, (int)s.Length);

            // load the ink into a new ink object
            // once an ink object has been "dirtied" it can never load ink again
            loadedInk.Load(isfBytes);

            return loadedInk;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private void LoadStrokes(Clock clock)
        {
            // read all the data into data table object for further querry
            DataTable dtPenStrokes = new DataTable();
            DataTable dtPoints = new DataTable();
            DataTable dtRects = new DataTable();
            DataTable dtPacketPoints = new DataTable();
            DataTable dtClock = new DataTable();

            string commandText = @"SELECT * FROM pen_stroke";
            SQLiteCommand command = new SQLiteCommand(commandText, sqlConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            
            dtPenStrokes.Load(reader);
            reader.Close();

            commandText = @"SELECT * FROM point";
            command = new SQLiteCommand(commandText, sqlConnection);
            reader = command.ExecuteReader();
            dtPoints.Load(reader);
            reader.Close();            

            commandText = @"SELECT * FROM rect";
            command = new SQLiteCommand(commandText, sqlConnection);
            reader = command.ExecuteReader();
            dtRects.Load(reader);
            reader.Close();

            commandText = @"SELECT * FROM packetpoint";
            command = new SQLiteCommand(commandText, sqlConnection);
            reader = command.ExecuteReader();
            dtPacketPoints.Load(reader);
            reader.Close();

            commandText = @"SELECT * FROM clock";
            command = new SQLiteCommand(commandText, sqlConnection);
            reader = command.ExecuteReader();
            dtClock.Load(reader);
            reader.Close();

            // create a penstroke object and fill up its required data
            List<PenStroke> strokes = new List<PenStroke>();
            int count = 0;
            try
            {
                // load the clock
                DataRow rowClock = dtClock.Rows[0];
                int clockX = int.Parse(rowClock[0].ToString());
                int clockY = int.Parse(rowClock[1].ToString());
                int clockR = int.Parse(rowClock[2].ToString());

                clock.X = clockX;
                clock.Y = clockY;
                clock.R = clockR;
                clock.numOfTrial = int.Parse(rowClock[5].ToString());

                // load pen stroke
                foreach (DataRow row in dtPenStrokes.Rows)
                {
                    PenStroke stroke = new PenStroke();
                    stroke.OrderID = count;
                    stroke.Guid = Guid.NewGuid().ToString();
                    
                    //bezier points
                    string[] bzpoints = row[1].ToString().Split(',');
                    for (int i = 0; i < bzpoints.Length - 1; i++)
                    {
                        DataRow rowPoint = dtPoints.Rows[int.Parse(bzpoints[i])-1];
                        stroke.BezierPoints.Add(new Point(int.Parse(rowPoint[2].ToString()),
                            int.Parse(rowPoint[3].ToString())));
                    }

                    //bounding box
                    int boundingbox = int.Parse(row[4].ToString())-1;
                    DataRow rowRect = dtRects.Rows[boundingbox];
                    stroke.BoundingBox = new System.Drawing.Rectangle(int.Parse(rowRect[1].ToString()),
                        int.Parse(rowRect[2].ToString()), int.Parse(rowRect[4].ToString()), int.Parse(rowRect[9].ToString()));

                    //combine to property
                    stroke.CombineTo = int.Parse(row[5].ToString());

                    //merge to property
                    stroke.MergeTo = int.Parse(row[6].ToString());

                    //merging rectangle
                    int mergingRect = int.Parse(row[7].ToString())-1;
                    rowRect = dtRects.Rows[mergingRect];
                    stroke.MergingRectangle = new System.Drawing.Rectangle(int.Parse(rowRect[1].ToString()),
                        int.Parse(rowRect[2].ToString()), int.Parse(rowRect[4].ToString()), int.Parse(rowRect[9].ToString()));

                    //packet points
                    string[] pkpoints = row[8].ToString().Split(',');
                    for (int i = 0; i < pkpoints.Length - 1; i++)
                    {
                        DataRow rowPkPoint = dtPacketPoints.Rows[int.Parse(pkpoints[i]) - 1];

                        int pointID = int.Parse(rowPkPoint[1].ToString())-1;
                        DataRow rowPoint = dtPoints.Rows[pointID];

                        PacketPoint pkPoint = new PacketPoint();
                        pkPoint.PkPt = new System.Drawing.Point(int.Parse(rowPoint[2].ToString()), int.Parse(rowPoint[3].ToString()));
                        pkPoint.Pressure = int.Parse(rowPkPoint[2].ToString());
                        pkPoint.TimeStamp = long.Parse(rowPkPoint[5].ToString());
                        stroke.PacketPoints.Add(pkPoint);
                    }

                    //pixel bounding box
                    int pixelBoundingBox = int.Parse(row[10].ToString())-1;
                    rowRect = dtRects.Rows[pixelBoundingBox];
                    stroke.PixelBoundingBox = new System.Drawing.Rectangle(int.Parse(rowRect[1].ToString()),
                        int.Parse(rowRect[2].ToString()), int.Parse(rowRect[4].ToString()), int.Parse(rowRect[9].ToString()));


                    //reco strokes
                    stroke.RecoStrokes = row[11].ToString();
                    stroke.isHand = int.Parse(row[12].ToString());
                    if (stroke.RecoStrokes != "")
                    {
                        if (int.Parse(stroke.RecoStrokes) < 0)
                        {
                            if (stroke.isHand <= 0) stroke.RecoStrokes = "?";
                            if (stroke.isHand == 1) stroke.RecoStrokes = "H1";
                            if (stroke.isHand == 2) stroke.RecoStrokes = "H2";
                        }
                    }
                    // timestamp
                    stroke.TimeStamp = long.Parse(row[13].ToString(), System.Globalization.NumberStyles.Float);
                    
                    strokes.Add(stroke);

                    count++;
                }
            }
            catch (Exception e)
            {

            }

            clock.PenStrokes = strokes;
        }





        // load each score
        /*
        private int[] loadScores()
        {
            DataTable dt = new DataTable();

            string commandText = @"SELECT score FROM score";
            int[] scores = new int[13];
            try
            {
                SQLiteCommand command = new SQLiteCommand(commandText, sqlConnection);

                SQLiteDataReader reader = command.ExecuteReader();
                dt.Load(reader);
                reader.Close();

                int i = 0;
                foreach (DataRow row in dt.Rows)
                {
                    scores[i] = int.Parse(row.ItemArray[0].ToString());
                    i++;
                }
            }
            catch (Exception e)
            {
            }
            return scores;
        }
        */

        #endregion

        #region display data on UI
        /// <summary>
        /// Todo: need a way to draw a exactly right circle and those inks
        /// </summary>
        /// <param name="loadedInk"></param>
        
        private void createButton(Clock oClock, string clockName)
        {
            Ink loadedInk = oClock.Ink.Clone();

            Pen pen = new System.Drawing.Pen(Color.Black, 1);

            // convert the ink to a bitmap and put it on pictureBox
            Bitmap curStrokesBitmap = new Bitmap(110, 110);
            Graphics g = Graphics.FromImage(curStrokesBitmap);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            Point ptRect = new System.Drawing.Point(this.pictureBox1.Width - 10, this.pictureBox1.Height - 10);
            this.pictureBox1.Renderer.PixelToInkSpace(g, ref ptRect);

            Renderer r = new Renderer();
            loadedInk.Strokes.ScaleToRectangle(new Rectangle(0, 0, ptRect.X, ptRect.Y));
            Microsoft.Ink.Stroke stroke = loadedInk.Strokes[0];


            r.Draw(g, loadedInk.Strokes);
            g.DrawEllipse(pen, 0, 0, this.pictureBox1.Width - 10, this.pictureBox1.Width - 10);

            int numOfClocks = dicClocks.Count - 1;

            // Create a Button for this clock
            Button newBt = new Button();
            newBt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            newBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            newBt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            newBt.Location = new System.Drawing.Point(numOfClocks * 120 + 10, 6);
            newBt.Name = clockName;
            newBt.Size = new System.Drawing.Size(110, 110);
            newBt.TabIndex = 1;
            newBt.Text = "";
            newBt.UseVisualStyleBackColor = true;
            newBt.Image = curStrokesBitmap;
            newBt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left))));
            newBt.Click += new EventHandler(newBt_Click);
            this.panel1.Controls.Add(newBt);
        }

        void newBt_Click(object sender, EventArgs e)
        {
            string name = ((Button)sender).Name;

            dispStrokesOnCanvas(this.dicClocks[name]); 

        }

        private void dispStrokesOnCanvas(Clock oClock) 
		{
            Ink loadedInk = oClock.Ink.Clone();

            Pen pen = new System.Drawing.Pen(Color.Black, 1);

            /// draw the circle and strokes on main animated picture box
            
            // Define the circle
            int radius = Math.Min(this.ClockDrawing.Width, this.ClockDrawing.Height) / 2 - 10;
            int drawingX = (this.ClockDrawing.Width - (2 * radius)) / 2;
            int drawingY = 10;

            // Get the drawing elements
            Bitmap canvasBitmap = new System.Drawing.Bitmap(this.ClockDrawing.Width, this.ClockDrawing.Height);
            Graphics g2 = Graphics.FromImage(canvasBitmap);//this.ClockDrawing.CreateGraphics();
            Ink disPlayInk = oClock.Ink.Clone();

            // calculate the translation and scale factor between the drawing circle and the circle from the database
            this.scale = (double)radius / (double)oClock.R;
            this.translationPt = new System.Drawing.Point((int)(drawingX - (oClock.X * scale)), (int)(drawingY - (oClock.Y * scale)));
            this.ClockDrawing.Renderer.PixelToInkSpace(g2, ref translationPt);

            disPlayInk.Strokes.Scale((float)scale, (float)scale);
            disPlayInk.Strokes.Move(translationPt.X, translationPt.Y);

            // Draw the circle
            g2.FillRectangle(Brushes.White, new System.Drawing.Rectangle(0, 0, this.ClockDrawing.Width, this.ClockDrawing.Height));
            g2.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            pen = new System.Drawing.Pen(Color.Black, 2);
            g2.DrawEllipse(pen, drawingX, drawingY, radius * 2, radius * 2);
            //g2.FillEllipse(Brushes.Red, drawingX + radius - 20, drawingY + radius - 2, 2, 2);
            this.ClockDrawing.Image = canvasBitmap;


            // put the strokes into the ink collector
            ic.Ink = disPlayInk;//loadedInk;

            Point tmpPt = new System.Drawing.Point(1, 1);
            this.ClockDrawing.Renderer.PixelToInkSpace(g2, ref tmpPt);

            //// Repaint the inkable region
            this.ClockDrawing.Invalidate();
		}
        
        private void dispScores(int[] scores)
        {
            for (int i = 0; i < scores.Length; i++)
            {
                int score = scores[i];
                switch (i)
                {
                    case 0:
                        if (score == 1)
                            check1.Visible = true;
                        break;
                    case 1:
                        if (score == 1)
                            check2.Visible = true;
                        break;
                    case 2:
                        if (score == 1)
                            check3.Visible = true;
                        break;
                    case 3:
                        if (score == 1)
                            check4.Visible = true;
                        break;
                    case 4:
                        if (score == 1)
                            check5.Visible = true;
                        break;
                    case 5:
                        if (score == 1)
                            check6.Visible = true;
                        break;
                    case 6:
                        if (score == 1)
                            check7.Visible = true;
                        break;
                    case 7:
                        if (score == 1)
                            check8.Visible = true;
                        break;
                    case 8:
                        if (score == 1)
                            check9.Visible = true;
                        break;
                    case 9:
                        if (score == 1)
                            check10.Visible = true;
                        break;
                    case 10:
                        if (score == 1)
                            check11.Visible = true;
                        break;
                    case 11:
                        if (score == 1)
                            check12.Visible = true;
                        break;
                    case 12:
                        if (score == 1)
                            check13.Visible = true;
                        break;
                }
            }
        }

        private void dispStrokeOrder(List<PenStroke> strokes)
        {
            int count = this.grpSequence.Controls.Count;
            for(int i=count-1;i>=0;i--)
            {
                this.grpSequence.Controls.Remove(this.grpSequence.Controls[i]);
            }
            
            int xLocation = 6;
            int id = 0;
            foreach (PenStroke stroke in strokes)
            {
                Label lbReco = new Label();
                // 
                // label15
                // 
                lbReco.AutoSize = true;
                lbReco.Cursor = System.Windows.Forms.Cursors.Hand;
                lbReco.Location = new System.Drawing.Point(xLocation, 30);
                lbReco.Name = id.ToString();
                lbReco.Size = new System.Drawing.Size(60, 30);
                lbReco.TabIndex = 0;
                lbReco.Text = stroke.RecoStrokes+",";
                lbReco.Tag = stroke.Guid;
                lbReco.MouseEnter += new EventHandler(lbReco_MouseEnter);
                lbReco.MouseLeave += new EventHandler(lbReco_MouseLeave);
                lbReco.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(lbReco_MouseDoubleClick);
                this.grpSequence.Controls.Add(lbReco);

                xLocation = xLocation + 20;
                id = id + 1;
            }
        }

        void lbReco_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            ((Label)sender).MouseLeave -= new EventHandler(lbReco_MouseLeave);
            ((Label)sender).MouseEnter -= new EventHandler(lbReco_MouseEnter);
            ((Label)sender).Visible = false;

            TextBox txtChangeBox = new TextBox();
            txtChangeBox.Location = ((Label)sender).Location;
            txtChangeBox.Size = ((Label)sender).Size;
            txtChangeBox.Focus();
            txtChangeBox.Name = ((Label)sender).Name;

            string outputtext = ((Label)sender).Text;
            if (outputtext.Contains(','))
            {
                int index = outputtext.IndexOf(',');
                outputtext = outputtext.Remove(index);
            }

            txtChangeBox.Text = outputtext;
            txtChangeBox.LostFocus += new EventHandler(txtChangeBox_LostFocus);
            txtChangeBox.KeyDown += new System.Windows.Forms.KeyEventHandler(txtChangeBox_KeyDown);
            this.grpSequence.Controls.Add(txtChangeBox);
            
        }

        void txtChangeBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            switch(e.KeyValue)
            {
                case 13:
                    this.txtChangeBox_LostFocus(sender, new EventArgs());
                    break;
            }
        }

        void txtChangeBox_LostFocus(object sender, EventArgs e)
        {
            foreach (Control control in this.grpSequence.Controls)
            {
                if (!control.Visible && control is Label)
                {
                    control.Visible = true;
                    ((Label)control).MouseLeave += new EventHandler(lbReco_MouseLeave);
                    ((Label)control).MouseEnter += new EventHandler(lbReco_MouseEnter);

                    string input = ((TextBox)sender).Text;
                    int newNum;
                    if (int.TryParse(input, out newNum))
                    {
                        input = input + ",";
                        if (input != ((Label)control).Text)
                        {
                            string guid = (string)((Label)control).Tag;
                            PenStroke editingSt = oClock.PenStrokes.Find(st => st.Guid == guid);
                            editingSt.ActualNumber = newNum;

                            Circle myCircle = new Circle(ClockDrawing.Width / 2 - 365, ClockDrawing.Height / 2 - 365, 365 * 2); 
                            // recalculate the score
                            ScoringNumbers currentScore = new ScoringNumbers(oClock, myCircle);
                            //score values
                            int[] scoreBoard = currentScore.scoringCPH();
                            dispScores(scoreBoard);
                        }

                        ((Label)control).Text = ((TextBox)sender).Text + ",";
                        ((Label)control).Name = ((TextBox)sender).Name;
                    }
                    else
                    {
                        MessageBox.Show("Please enter a number!!");
                    }

                    break;
                }
            }

            this.grpSequence.Controls.Remove((TextBox)sender);
        }

        private void reportAirTime(List<PenStroke> penStrokes)
        {
            long preAirtime = 0;
            Series seriesAirTime = this.chartAirTime.Series[0];
            int i = -1;
            foreach (PenStroke stroke in penStrokes)
            {
                i++;
                int lastIndex = stroke.PacketPoints.Count-1;
                if (preAirtime == 0)
                {                    
                    preAirtime = stroke.PacketPoints[lastIndex].TimeStamp;
                    continue;
                }

                if (stroke.MergeTo < 100 && stroke.MergeTo < i)
                {
                    preAirtime = stroke.PacketPoints[lastIndex].TimeStamp;
                    continue;
                }

                long curAirTime = stroke.PacketPoints[0].TimeStamp;
                long timeDiff = curAirTime - preAirtime;
                object[] addingY = new object[]{timeDiff};
                seriesAirTime.Points.AddXY(stroke.RecoStrokes, addingY);
                preAirtime = stroke.PacketPoints[lastIndex].TimeStamp;
            }
        }

        private void reportPressure(List<PenStroke> penStrokes)
        {
            foreach (PenStroke stroke in penStrokes)
            {
                foreach (PacketPoint pkPoint in stroke.PacketPoints)
                {
                    Series seriesPkPoint = this.chartPressure.Series[0];
                    object[] addingY = new object[]{pkPoint.Pressure};
                    seriesPkPoint.Points.AddXY(stroke.RecoStrokes, addingY);
                }
            }
        }

        #endregion
        
        // Read each score from the file
        private float[] readAirtime(char[] delimiters, String tempString)
        {
            int i = 0, j = 0;
            String[] subAir = tempString.Split(delimiters, StringSplitOptions.None);
            airtime = new float[subAir.Length / 4];
            float[] tmpAirtime = new float[subAir.Length / 4];

            foreach (String subs in subAir)
            {
                if ((i > 4) && (i % 4 == 1))
                {
                    airtime[j] = float.Parse(subs);
                    tmpAirtime[j] = float.Parse(subs);
                    j++;
                }
                i++;
            }
            return tmpAirtime;
        }        

        private void clearScoreboard()
        {
            check1.Visible = false;
            check2.Visible = false;
            check3.Visible = false;
            check4.Visible = false;
            check5.Visible = false;
            check6.Visible = false;
            check7.Visible = false;
            check8.Visible = false;
            check9.Visible = false;
            check10.Visible = false;
            check11.Visible = false;
            check12.Visible = false;
            check13.Visible = false;
        }

        // Display participant information.
        private StringBuilder genParInfo(String filename)
        {            
            StringBuilder tmpStringBuilder = new StringBuilder("");

            String tempString = "Name: " + filename + Environment.NewLine;
            tmpStringBuilder.Append(tempString);


            // Calculate total score
            int tot_score = calcTotScore();

            tempString = "Total Score: " + tot_score.ToString() + " / 13" + Environment.NewLine;
            tmpStringBuilder.Append(tempString);

            // Calculate average airtime
            float avg_airtime = calcAvgAir();

            tempString = "Average airtime: " + avg_airtime.ToString() + " sec" + Environment.NewLine;
            tmpStringBuilder.Append(tempString);
                      
            return tmpStringBuilder;
        }

        // Calculate total score
        private int calcTotScore()
        {
            int temp_score = 0;

            foreach (int score in scoreboard)
            {
                temp_score += score; 
            }

            return temp_score;
        }

        // Calculate average airtime
        private float calcAvgAir()
        {
            float temp_air = 0;

            foreach (float air in airtime)
            {
                temp_air += air;
            }

            temp_air /= airtime.Length;

            return temp_air;
        }

        int packetPtId = 0;
        int strokeID = 0;
        Microsoft.Ink.Ink renderInk;

        Point[] pkPoints;
        List<Microsoft.Ink.Stroke> existingStrokes = new List<Microsoft.Ink.Stroke>();
        Microsoft.Ink.InkOverlay mInkOverly;
        private static DateTime JanFirst1970 = new DateTime(1970, 1, 1);
        long startTime;
        long pauseTime;
        long oriStartTime;
        //void ClockDrawing_Paint(object sender, PaintEventArgs e)
        private void Animation()
        {
            try
            {
                Clock[] tmpClock = new Clock[dicClocks.Count];
                dicClocks.Values.CopyTo(tmpClock, 0);

                if (tmpClock != null && tmpClock[0] != null)
                {
                    if (isPlaying)
                    {
                        //Strokes strokes = tmpClock[0].Ink.Strokes;
                        if (tmpClock.Length > 0 && tmpClock[0] != null)
                        {
                            Strokes oriStorkes = tmpClock[0].Ink.Strokes;
                            List<PenStroke> strokes = tmpClock[0].PenStrokes;
                            using (Graphics g = this.ClockDrawing.CreateGraphics())
                            {
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                                Renderer r = new Renderer();
                                Ink loadingInk = new Ink();

                                Microsoft.Ink.DrawingAttributes da = new Microsoft.Ink.DrawingAttributes();
                                ExtendedProperties inkProperties = ic.Ink.ExtendedProperties;


                                if (packetPtId == 0)
                                {

                                    if (strokeID == 0)
                                    {
                                        ic.Ink.DeleteStrokes(ic.Ink.Strokes);
                                        this.ClockDrawing.Invalidate();
                                        startTime = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);
                                        oriStartTime = strokes[strokeID].PacketPoints[packetPtId].TimeStamp;

                                    }
                                    long oriPkgPtTime = strokes[strokeID].PacketPoints[packetPtId].TimeStamp;
                                    long now = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);
                                    long fromStart = now - startTime;
                                    this.BeginInvoke(new AnimationDelegate(DisplaySec), new object[] { fromStart });

                                    // create a new stroke
                                    if (now - startTime >= oriPkgPtTime - oriStartTime)
                                    {
                                        createNewStroke(g, strokes, oriStorkes, inkProperties);
                                    }
                                }
                                else
                                {
                                    long now = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);

                                    long difference = now - startTime;
                                    Microsoft.Ink.Stroke renderingStroke = ic.Ink.Strokes[ic.Ink.Strokes.Count - 1];

                                    int tmpPckPtId = 0;
                                    int pckPtCount = strokes[strokeID].PacketPoints.Count;
                                    System.Drawing.Point finalPktPt = new System.Drawing.Point();
                                    while (true)
                                    {
                                        long oriPkgPtTime = strokes[strokeID].PacketPoints[packetPtId].TimeStamp;

                                        if (difference >= oriPkgPtTime - oriStartTime && packetPtId != pckPtCount)
                                        {
                                            System.Drawing.Point tmpOriPt = oriStorkes[strokeID].GetPoint(packetPtId);
                                            finalPktPt = new System.Drawing.Point((int)(tmpOriPt.X * scale) + translationPt.X, (int)(tmpOriPt.Y * scale) + translationPt.Y);

                                            renderingStroke.SetPoint(packetPtId, finalPktPt);

                                            packetPtId++;
                                            if (packetPtId == pckPtCount) break;
                                        }
                                        else if (packetPtId != pckPtCount && tmpPckPtId < pckPtCount && finalPktPt != new Point(0,0))
                                        // set the rest of the points in the stroke as the current end point
                                        {
                                            if (tmpPckPtId < packetPtId) tmpPckPtId = packetPtId;
                                            renderingStroke.SetPoint(tmpPckPtId, finalPktPt);

                                            tmpPckPtId++;
                                        }
                                        else
                                            break;
                                    }

                                    Microsoft.Ink.Stroke tmpStroke = ic.Ink.Strokes[ic.Ink.Strokes.Count - 1];
                                    Point upperLeft = new System.Drawing.Point(tmpStroke.GetBoundingBox().X, tmpStroke.GetBoundingBox().Y);
                                    Point bottomRight = new System.Drawing.Point(tmpStroke.GetBoundingBox().Right, tmpStroke.GetBoundingBox().Bottom);
                                    this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref upperLeft);
                                    this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref bottomRight);

                                    this.ClockDrawing.Invalidate(new Rectangle(upperLeft.X, upperLeft.Y, bottomRight.X, bottomRight.Y));

                                    if (strokes[strokeID].PacketPoints.Count == packetPtId)
                                    {
                                        packetPtId = 0;
                                        strokeID++;
                                        if (strokeID == strokes.Count)
                                        {
                                            strokeID = 0;
                                            isPlaying = false;
                                            Application.Idle -= new EventHandler(Application_Idle);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //Point scaleRectangle = new System.Drawing.Point(this.ClockDrawing.Height, this.ClockDrawing.Height);
                        //Strokes staticStrokes = tmpClock[0].Ink.Strokes;
                        //staticStrokes.ScaleToRectangle(new Rectangle(0, 0, scaleRectangle.X, scaleRectangle.Y));
                        //staticStrokes.Scale(0.85f, 0.85f);
                        ////staticStrokes.Move(1750, 800);
                        //Graphics g2 = this.ClockDrawing.CreateGraphics();
                        //Renderer r = new Renderer();
                        //r.Draw(g2, staticStrokes);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void createNewStroke(Graphics g, List<PenStroke> strokes, Strokes oriStorkes, ExtendedProperties inkProperties)
        {
            pkPoints = new Point[strokes[strokeID].PacketPoints.Count];
            System.Drawing.Point tmpPt = oriStorkes[strokeID].GetPoint(packetPtId);
            System.Drawing.Point finalPktPt = new System.Drawing.Point((int)(tmpPt.X * scale) + translationPt.X, (int)(tmpPt.Y * scale) + translationPt.Y);

            Point newPt = new Point((int)(tmpPt.X * 0.8) - 500, (int)(tmpPt.Y * 0.8) - 5500);
            int[] pressureValue = new int[strokes[strokeID].PacketPoints.Count];
            for (int i = 0; i < pkPoints.Length; i++)
            {
                pkPoints[i] = finalPktPt;
            }

            Microsoft.Ink.Stroke tmpStroke = ic.Ink.CreateStroke(pkPoints);// mInkOverly.Ink.CreateStroke(pkPoints);
            if (inkProperties.DoesPropertyExist(PacketProperty.NormalPressure))
            {
                tmpStroke.SetPacketValuesByProperty(PacketProperty.NormalPressure, pressureValue);
            }

            Point upperLeft = new System.Drawing.Point(tmpStroke.GetBoundingBox().X, tmpStroke.GetBoundingBox().Y);
            Point bottomRight = new System.Drawing.Point(tmpStroke.GetBoundingBox().Right, tmpStroke.GetBoundingBox().Bottom);
            this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref upperLeft);
            this.ClockDrawing.Renderer.InkSpaceToPixel(g, ref bottomRight);

            this.ClockDrawing.Invalidate(new Rectangle(upperLeft.X, upperLeft.Y, bottomRight.X, bottomRight.Y));

            packetPtId++;
        }

        private delegate void AnimationDelegate(long sec);
        private void DisplaySec(long sec)
        {
            Sec.Text = (sec/1000).ToString();
            double percent = ((double)sec/1000) / (double)DurationInSecond;
            this.trackBar1.Value = (int)(percent * 100) > 100 ? 100 : (int)(percent * 100);

        }
    }
}