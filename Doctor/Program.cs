﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ClockReader
{
    static class Program
    {

        /// <summary>
        /// 
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {

                Application.EnableVisualStyles();
                Application.Run(new ClockReader());
            }
            catch (Exception ex)
            {

            }
        }
    }
}
