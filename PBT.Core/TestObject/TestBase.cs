﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PBT.Core.TestObject
{
    public class TestBase
    {
        private Microsoft.Ink.Ink ink;
        private List<PenStroke> penstrokes;

        public TestBase()
        {
            penstrokes = new List<PenStroke>();            
        }

        public List<PenStroke> PenStrokes
        {
            get
            {
                return this.penstrokes;
            }
            set
            {
                this.penstrokes = value;
            }
        }

        public Microsoft.Ink.Ink Ink
        {
            set
            {
                this.ink = value;
            }
            get
            {
                return this.ink;
            }
        }
    }
}
