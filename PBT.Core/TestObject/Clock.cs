﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PBT.Core.TestObject
{
    public class Clock : TestBase
    {
        private int[] scores;
        private float[] airtimes;
        private StringBuilder parInfo;        

        public int numOfTrial { get; set; }
        public Clock() : base()
        {
            scores = new int[13];
            parInfo = new StringBuilder("");            
        }

        /// <summary>
        /// set or get the scores from the recorded data
        /// </summary>
        public int[] ScoreBoard
        {
            set
            {
                scores = value;
            }
            get
            {
                return this.scores;
            }
        }

        public float[] AirTime
        {
            set
            {
                this.airtimes = value;
            }
            get
            {
                return this.airtimes;
            }
        }       

        public int X { set; get; }
        public int Y { set; get; }
        public int R { set; get; }
    }
}
